﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class wall : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void OnTriggerEnter(Collider other) {
		if (other.tag == "sword" && playercontroller.instance.h) {
			Destroy(this.gameObject);
			ParticleSystem ps = playercontroller.instance.wallFall.GetComponent<ParticleSystem> ();
			var tmp = ps.shape;
			tmp.enabled = true;
			tmp.shapeType = ParticleSystemShapeType.Mesh;
			tmp.mesh = this.gameObject.GetComponent<MeshFilter> ().mesh;
			//playercontroller.instance.wallFall.GetComponent<ParticleSystem> ().shape = tmp;
			Instantiate (playercontroller.instance.wallFall, transform.position, Quaternion.Euler(new Vector3(-90, 0, 0)));
		}
	}
}
