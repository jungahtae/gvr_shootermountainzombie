﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class monster : MonoBehaviour {

	// Use this for initialization

	public Vector3 target;

	public float speed = 5;

	public float runSpeed = 5;

	public float walkSpeed = 0.7f;

	public float turnSpeed = 80;

	public float jumpSpeed = 50;

	public float gravity = 9.81f;

	public CharacterController controller;

	public Vector3 moveDirection = Vector3.zero;

	public Animator anim;

	public bool isStatic = false;

	public float timer = 1;

	public float rg = 10;

	public bool didUp = false;

	public bool setHit;

	public float health = 100;

	public GameObject healthBar;

	public bool l = true;

	void Start () {
		StartCoroutine (StopDidUp());
		target = transform.position;
		anim = GetComponent<Animator> ();
		controller = GetComponent<CharacterController> ();
	}

	void FindTarget() {
		Vector3 tmpTarget;
		Quaternion euler = transform.rotation;
		int count = 0;
		while (true) {
			count++;
			if (count == 100)
				return;
			transform.Rotate (new Vector3 (0, 30 * Random.Range(0, 17), 0));
			Vector3 origin = transform.position;
			origin.y = 0.7f;
			tmpTarget = origin + transform.forward * 5;
			Debug.DrawLine (origin, tmpTarget, Color.blue, 10);
			if (Physics.Raycast (origin, tmpTarget - origin, 10, 1 << 10))
				continue;
			target = tmpTarget;
			return;
		}
		transform.rotation = euler;
	}

	public void CheckForPlayer() {
		if (didUp == false)
			return;
		Vector3 tmpTarget;
		Quaternion euler = transform.rotation;

		for (float i = -9; i <= 9; i += 0.5f) {
			if (Vector3.Distance (transform.position, target) > 0.2f) {
				if (i < -4 || i > 4)
					continue;
			}
			float rotateAngle = i * 10;
			transform.rotation = euler;
			transform.Rotate (new Vector3 (0, rotateAngle, 0));
			tmpTarget = transform.position + transform.forward * 5;
			Vector3 orgin = transform.position;
			orgin.y = 0.7f;
			Debug.DrawRay (orgin, tmpTarget - transform.position, Color.blue, 2);
			if (Physics.Raycast (orgin, tmpTarget - transform.position, Mathf.Infinity, 1 << 11)) {
				target = playercontroller.instance.transform.position;
				anim.SetBool ("run", true);
				speed = runSpeed;
				return;
			}
			//target = tmpTarget;
		}
		transform.rotation = euler;
	}


	IEnumerator StopDidUp() {
		didUp = false;
		yield return new WaitForSeconds (0.7f);
		didUp = true;
	}

	IEnumerator HitPlayerCo() {
		anim.Play ("hit");
		yield return new WaitForSeconds (1.2f);
		setHit = false;
	}
	// Update is called once per frame

			//show me how it work

			// 1 scriptt on player
			// 1 script on enemy
			// lol
    IEnumerator MoveDown()
    {
        yield return new WaitForSeconds(3);
        while (true)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y - 0.5f *  Time.deltaTime, transform.position.z);
            if (transform.position.y < -1)
            {
                DestroyImmediate(transform.gameObject);
                yield break;
            }
            yield return null;
        }
    }

    IEnumerator DeactiveGameObjectAfterCR(GameObject obj, float time)
    {
        yield return new WaitForSeconds(time);
        obj.gameObject.SetActive(false);
    }

    void DeactiveGameObjectAfter(GameObject obj, float time)
    {
        StartCoroutine(DeactiveGameObjectAfterCR(obj, time));
    }

	void Update () {

	    healthBar.transform.GetChild (0).localScale = new Vector3 (health / 100, healthBar.transform.GetChild (0).localScale.y, healthBar.transform.GetChild (0).localScale.z);

		if (health <= 0 && health >= -1000) {
            spawn.instance.score++;
            health = -2000;
            if (spawn.instance.score > spawn.instance.highestScore && playercontroller.instance.health > 0)
            {
                spawn.instance.highestScore = spawn.instance.score;
                PlayerPrefs.SetInt("highestScore", spawn.instance.score);
            }
			l = false;
			anim.Play ("dying");
            StartCoroutine(MoveDown());
            playercontroller.instance.timeCombo = 1.8f;
            playercontroller.instance.combo++;
            if (playercontroller.instance.combo > spawn.instance.bestCombo && playercontroller.instance.health > 0)
            {
                spawn.instance.bestCombo = playercontroller.instance.combo;
                PlayerPrefs.SetInt("bestCombo", spawn.instance.bestCombo);
            }
            shopmanager.instance.combo.text = "COMBO x" + playercontroller.instance.combo.ToString();
            StopCoroutine("DeactiveGameObjectAfterCR");
            DeactiveGameObjectAfter(shopmanager.instance.combo.transform.parent.gameObject, 2);
            shopmanager.instance.combo.transform.parent.gameObject.SetActive(false);
            shopmanager.instance.combo.transform.parent.gameObject.SetActive(true);
            transform.GetChild(2).GetComponent<Animator>().Play("fade");
			return;
		}

        if (health <= 0)
            anim.Play("dying");

        if (setHit) {
			StartCoroutine (HitPlayerCo());
			return;
		}

		CheckForPlayer ();

		if (runSpeed == speed && Vector3.Distance (transform.position, playercontroller.instance.transform.position) < 2 && health >= 0) {
			setHit = true;
		}

		if (target.y != transform.position.y)
			target.y = transform.position.y;
		
		transform.LookAt (target);

		if (didUp) {
			if (Vector3.Distance (target, transform.position) < 0.2f) {
				speed = walkSpeed;
				anim.SetBool ("run", false);
				transform.position = target;
				moveDirection = Vector3.zero;
				StartCoroutine (StopDidUp());
			} else moveDirection = (target - transform.position).normalized * speed;
		}

		moveDirection.y -= gravity * Time.deltaTime * 5;

        if (health >= 0)
		    controller.Move(moveDirection * Time.deltaTime);

		timer -= Time.deltaTime;
		if (timer < 0) {
			timer = rg;
			if (isStatic == false)
				FindTarget ();
		}

		Vector3 horizontalVelocity = controller.velocity;
		horizontalVelocity = new Vector3(controller.velocity.x, 0, controller.velocity.z);
		float horizontalSpeed = horizontalVelocity.magnitude;
		float verticalSpeed = controller.velocity.y;
		float overallSpeed = controller.velocity.magnitude;

		//anim.SetFloat("VelocityX", Input.GetAxis("Vertical"));
		//anim.SetFloat("VelocityZ", Input.GetAxis("Horizontal"));
		anim.SetFloat("speed", horizontalSpeed);
	}

	public void OnTriggerEnter(Collider other) {
		if (other.tag == "sword" && playercontroller.instance.h) {
			if (health <= 0) {
				return;
			}
			float k = (shopmanager.instance.levelSword) * 25f;
			if (k == 100)
				k = 100;
			health -= k;
			if (health <= 0) {
				shopmanager.instance.cur++;
				shopmanager.instance.PlayBear ();
			}

		}
	}
}
