﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawn : MonoBehaviour {

	// Use this for initialization

	public static spawn instance = null;

	public int levelx = 0;

	public GameObject monster;

    public GameObject monster2;

	public string username = "";

	public int score, highestScore, bestCombo;

    public int maxMonster;

    public float timeSpawn;

	void Awake() {
		if (instance == null)
			instance = this;
		else {
			DestroyImmediate (this.gameObject);
			return;
		}
		score = 0;
        highestScore = PlayerPrefs.GetInt("highestScore");
        bestCombo = PlayerPrefs.GetInt("bestCombo");
		username = "Player" + Random.Range (0, 99999).ToString();
		DontDestroyOnLoad (this.gameObject);
	}

	void Start () {
		for (int i = 0; i < 10; i++) {
			Instantiate (monster, new Vector3(Random.Range(-20, 20), 3, Random.Range(0, 20)), Quaternion.identity);
		}
        timeSpawn = 15;
	}

    public void SpawnAMonsterBatch()
    {
        int num;
        if (Time.time < 60)
            num = 5;
        else num = Mathf.RoundToInt(Time.time / 60) * 5;
        for (int i = 1; i < num; i++)
        {
            Instantiate(monster, new Vector3(Random.Range(-20, 20), 3, Random.Range(0, 20)), Quaternion.identity);
        }
    }

	void OnLevelWasLoaded(int level) {
	}
	
	// Update is called once per frame
	void Update () {
        timeSpawn -= Time.deltaTime;
		if (timeSpawn <= 0) {
            timeSpawn = 15;
            SpawnAMonsterBatch();
        }
	}
}
