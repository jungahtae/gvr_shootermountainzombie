﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playercontroller : MonoBehaviour {

	// Use this for initialization

	public static playercontroller instance = null;

	public Vector3 target;

	public float speed = 5;

	public float turnSpeed = 80;

	public float jumpSpeed = 50;

	public float gravity = 9.81f;

	public CharacterController controller;

	public Vector3 moveDirection = Vector3.zero;

	public Animator anim;

	public float health = 2000;

	public GameObject healthBar;

	public GameObject Shop;

	public bool h = false;

	public GameObject wallFall;

    public GameObject gun, particleSystem;

    public float timeCombo;

    public int combo;

    public void Restart()
    {
        Application.LoadLevel(0);
    }

	void Awake()
	{
		instance = this;
	}

	public void DidBuy(int i) {
		if (i == 2) {
			transform.GetChild (3).gameObject.SetActive(true);
		}
		if (i == 1) {
			transform.GetChild (4).gameObject.SetActive(true);
		}
	}

	public void OnTriggerEnter(Collider other) {
		if (other.tag == "monsterhit") {
			//Time.timeScale = 0;
			//if (other.transform.parent.parent.parent.parent.parent.parent.parent.parent.GetComponent<monster> ().setHit) {
			float k = (4 - shopmanager.instance.levelShield) * 25f;
			Debug.Log (k);
			if (k == 0)
				k = 5;
			Debug.Log (1);
			if (other.transform.parent.parent.parent.parent.parent.parent.parent.parent.GetComponent<monster> ().l == false)
				return;
			Debug.Log (2);
			health -= k;
			//}
		}

		if (other.tag == "master") {
			shopmanager.instance.transform.GetChild (1).gameObject.SetActive (true);
		}

		if (other.tag == "WIN") {
			shopmanager.instance.clear = true;
			shopmanager.instance.transform.GetChild (2).gameObject.SetActive (true);
			shopmanager.instance.Win ();
		}

	}

	public void OnTriggerExit(Collider other) {
		if (other.tag == "master") {
			shopmanager.instance.transform.GetChild (1).gameObject.SetActive (false);
		}
	}

	void Start () {
		anim = GetComponent<Animator> ();
		controller = GetComponent<CharacterController> ();
		//transform.GetChild (4).gameObject.SetActive(false);
		//transform.GetChild (3).gameObject.SetActive(false);
	}
	
	// Update is called once per frame

	// this is my script man, it is very short
	// this is qll the script :O
	// woooow 
	// see the enemy script

	IEnumerator HCo() {
		h = true;
		yield return new WaitForSeconds (3);
		h = false;
	}

    void Update()
    {
        timeCombo -= Time.deltaTime;
        if (timeCombo <= 0)
            combo = 0;

        if (health <= 0 && health > -2000)
        {
            GetComponent<Animator>().Play("lie");
            shopmanager.instance.gameoverCanvas.SetActive(true);
            health = -2000;
        }
    }
}
