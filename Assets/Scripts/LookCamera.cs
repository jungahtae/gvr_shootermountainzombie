﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookCamera : MonoBehaviour {

	// Use this for initialization
	public GameObject camera;

	void Start () {
		camera = Camera.main.gameObject;
	}
	
	// Update is called once per frame
	void LateUpdate () {
		transform.LookAt (camera.transform);
		transform.rotation = Quaternion.Euler (new Vector3(0, transform.rotation.eulerAngles.y, transform.rotation.eulerAngles.z));
	}
}
