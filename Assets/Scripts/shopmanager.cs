﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class shopmanager : MonoBehaviour {

	// Use this for initialization

	public static shopmanager instance = null;

	public Text coinTxt, coinsNeededSword, coinsNeededShield, target, combo, bestCombo, bestComboGV, bestScoreGV, timePLGV;

    public GameObject gameoverCanvas;

	public int coins;

	public bool shield, sword;

	public int levelShield, levelSword;

	public int cur;

	public GameObject[] sBrs, sWBrs;

	public Slider sound, music;

	public float sd, ms;

	public AudioSource soundAS, musicAS;

	public AudioClip bear;

	public Text levelTxt;

	public Text clock, scoreTxt, username;

	public float time;

	public bool clear = false;

    public Image healthBar;

	public void Win() {
		StartCoroutine (ScoreCo());
	}

	public struct Player {
		public int score;
		public string name;
		public Player(int a, string b) {
			score = a;
			name = b;
		}
	}

	IEnumerator ScoreCo() {
		if (spawn.instance.levelx == 1)
			shopmanager.instance.transform.GetChild (2).gameObject.SetActive (false);
		int z = Random.Range (200, 300 * (spawn.instance.levelx + 1));
		while (true) {
			time -= 2 * 20;
			spawn.instance.score += z;
			if (time <= 0)
				time = 0;
			UpClock ();
			scoreTxt.text = "Score: " + spawn.instance.score.ToString ();
			if (time <= 0)
				break;
			yield return new WaitForSeconds (0.01f);
		}
		if (spawn.instance.levelx == 1) {
			shopmanager.instance.transform.GetChild (12).gameObject.SetActive (true);
			UpBest ();
		};
		yield break;
	}

	public void UpBest() {
		int num = PlayerPrefs.GetInt ("num");
		num++;
		PlayerPrefs.SetInt (num.ToString(), spawn.instance.score);
		PlayerPrefs.SetString (num.ToString() + "n", spawn.instance.username);
		PlayerPrefs.SetInt ("num", num);

		List<Player> p = new List<Player> ();
		for (int i = 1; i <= num; i++)
			p.Add (new Player (PlayerPrefs.GetInt (i.ToString ()),
				PlayerPrefs.GetString (i.ToString () + "n")));
		
		p.Sort ((Player x, Player y) => (y.score.CompareTo(x.score)));

		for (int i = 1; i <= 10; i++) {
			if (i > num) {
				shopmanager.instance.transform.GetChild (12).GetChild (i + 3).gameObject.SetActive (false);
				continue;
			}
			shopmanager.instance.transform.GetChild (12).GetChild (i + 3).GetComponent<Text>().text = "Rank " + i.ToString() + ": Score " + p[i - 1].score + " " + p[i - 1].name;
		}

	}

	void Start () {
		clear = false;
		username.text = spawn.instance.username;
		sound.value = 1;
		music.value = 1;
		instance = this;
		coins = 50000;
		levelShield = 4;
		levelSword = 0;
		InitTest ();
		Up ();
	}

	public void Retart() {
		Application.LoadLevel (0);
	}

	public void OnSoundChange() {
		soundAS.volume = sound.value;
	}

	public void OnMusicChange() {
		musicAS.volume = music.value;
	}

	public void PlayBear() {
		soundAS.PlayOneShot (bear);
	}

	public void Buy(int i) {
		if (i == 2) {
			if (coins < 50)
				return;
			coins -= 50;
			shield = true;
			playercontroller.instance.DidBuy (2);
		}
		if (i == 1) {
			if (coins < 100)
				return;
			coins -= 100;
			sword = true;
			playercontroller.instance.DidBuy (1);
		}
	}

	public void Upgrade(int i) {
		if (i == 2) {
			if (shield == false)
				return;
			int z = levelShield;
			if (z >= 4)
				z = 4;
			z = (z + 1) * 20;
			if (coins <= z)
				return;
			coins -= z;
			levelShield++;
		}
		if (i == 1) {
			if (sword == false)
				return;
			int z = levelSword;
			if (z >= 4)
				z = 4;
			z = (z + 1) * 50;
			if (coins <= z)
				return;
			coins -= z;
			levelSword++;
		}
		if (levelSword >= 4)
			levelSword = 4;
		if (levelShield >= 4)
			levelShield = 4;
		Up ();
	}

	public void InitTest() {
	}

	public void Up() {
		for (int i = 0; i < 5; i++) {
	//		sBrs [i].gameObject.SetActive (false);
			sWBrs [i].gameObject.SetActive (false);
	//		if (i <= levelShield)
	//			sBrs [i].gameObject.SetActive (true);
			if (i <= levelSword)
				sWBrs [i].gameObject.SetActive (true);
		}
	}

	public void LoadLevel() {
		spawn.instance.levelx++;
		Application.LoadLevel (0);

	}
	public string Round0(int t) {
		if (t < 10)
			return "0" + t.ToString ();
		else
			return t.ToString ();
	}


	public void UpClock() {

		int h, m, s;
        
		h = (int) (time / 60 / 60);

		m = (int) ((time - h * 60 * 60) / 60);

		s = (int) (time - h * 60 * 60 - m * 60);

		timePLGV.text = clock.text = "Time: " + Round0 (h) + ":" + Round0 (m) + ":" + Round0 (s);

        bestScoreGV.text = username.text = "Highest Score: " + spawn.instance.highestScore.ToString();

        levelTxt.text = "Score: " + spawn.instance.score.ToString();

        bestComboGV.text = bestCombo.text = "Best Combo: " + spawn.instance.bestCombo.ToString();

	}

	// Update is called once per frame
	void Update () {
		//coinTxt.text = coins.ToString ();

        /*
		if (clear == false) {
			playercontroller.instance.health = 0;
			playercontroller.instance.anim.Play ("lie");
			shopmanager.instance.transform.GetChild (5).gameObject.SetActive(true);
			return;
		}*/

        if (playercontroller.instance.health > 0)
		    time += Time.deltaTime;

		scoreTxt.text = "Score: " + spawn.instance.score.ToString ();

        healthBar.transform.localScale = new Vector3(Mathf.Max(0, playercontroller.instance.health / 500), 1, 1);

		UpClock ();

		if (cur >= 10 + spawn.instance.levelx * 30) {
			transform.GetChild (3).gameObject.SetActive (true);;
		}

		target.text = "Moods Kil Target: " + cur.ToString () + " / " + (10 + spawn.instance.levelx * 30).ToString();


		int z = levelShield;
		if (z >= 4)
			z = 4;
		z = (z + 1) * 20;

		//coinsNeededShield.text = (z).ToString ();

		z = levelSword;
		if (z >= 4)
			z = 4;
		z = (z + 1) * 50;

		//coinsNeededSword.text = (z).ToString ();
	}
}
